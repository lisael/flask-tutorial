# Trax

A flask tutorial

## Contirbuting

### System setup

<details>
<summary>Linux</summary>

GNU coreutils are probably already installed.

Install python >=3.8

</details>


<details>
<summary>Mac OS</summary>

Install python and GNU utils:

```sh
brew python install coreutils ed findutils gawk gnu-sed gnu-tar grep make
```

Assuming you have a fairly standard Terminal/shell environment, and assuming that you want to use the GNU versions instead of the BSD versions for everything you’ve installed with Homebrew, you can append the following to your ~/.profile file.

```sh
# Get list of gnubin directories
export GNUBINS="$(find /usr/local/opt -type d -follow -name gnubin -print)";

for bindir in ${GNUBINS[@]}; do
  export PATH=$bindir:$PATH;
done;
``` 
</details>

