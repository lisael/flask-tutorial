# Get the dir of the current Makefile
MAKEFILE_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

## System python to use to bootstrap the virtualenv
SYS_PYTHON?=python
## Virtualenv dir
VENV?=venv
## Main source dir
SRC_DIR?=$(MAKEFILE_DIR)/src
## Development server host
SERVER_HOST?=localhost
## Development server port
SERVER_PORT?=5000

VENV_BIN=$(VENV)/bin
PIP=$(VENV_BIN)/pip
PYTHON=$(VENV_BIN)/python
REQUIREMENTS=requirements.txt
DEV_REQUIREMENTS=dev_requirements.txt

define PRINT_HELP_PYSCRIPT
import re, sys

vars = []
var_help = None
print("TARGETS:")
for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_/-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("  %-20s %s" % (target, help))
	else:
		match = re.match(r'^## (.*)$$', line)
		if match:
			var_help = match.groups()[0]
		else:
			match = re.match(r'([a-zA-Z_-]+)\?=(.*)$$', line)
			if match and var_help:
				vars.append(tuple(match.groups()) + (var_help,))
				var_help = None
if vars:
	print()
	print("VARIABLES (Override with: `VAR_NAME=value make <target>):")
	for name, default, help in vars:
		print("  %-20s %s (%s)" % (name, help, default))
endef
export PRINT_HELP_PYSCRIPT

.PHONY: help
.DEFAULT: help
help:  ## Show this help
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

.INTERMEDIATE: develop
.PHONY: develop
develop: virtualenv requirements dev_requirements  ## Create a developent environment

.INTERMEDIATE: virtualenv
.PHONY: virtualenv
virtualenv: $(VENV)/bin/python  ## Create the virtualenv

$(VENV)/bin/python:
	$(SYS_PYTHON) -m venv $(VENV)

.INTERMEDIATE: requirements
.PHONY: requirements
requirements: $(VENV)/requirements.txt  ## Install or update runtime equirements

$(VENV)/requirements.txt: $(VENV) $(REQUIREMENTS)
	$(PIP) install -U -r $(REQUIREMENTS)
	cp $(REQUIREMENTS) $(VENV)

.INTERMEDIATE: dev_requirements
.PHONY: requirements
dev_requirements: $(VENV)/dev_requirements.txt  ## Install or update runtime equirements
	
$(VENV)/dev_requirements.txt: $(VENV) $(DEV_REQUIREMENTS)
	$(PIP) install -U -r $(DEV_REQUIREMENTS)
	cp $(DEV_REQUIREMENTS) $(VENV)

.PHONY: run
run: develop  ## Run the development server
	. ./env.dev && flask run -h $(SERVER_HOST) -p $(SERVER_PORT)

.PHONY: smoke-test
smoke-test: dev_requirements  ## Run a smoke test on the root of the service
	http GET "$(SERVER_HOST):$(SERVER_PORT)/"

.PHONY: clean
clean: clean-build clean-pyc ## remove all build and Python artifacts

.PHONY: clean-build
clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.PHONY: clean-pyc
clean-pyc: ## remove Python file artifacts
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
	
.PHONY: reset
reset: clean  ## Reset all (including the virtualenv)
	rm -rf $(VENV)
